package apfo.testmultifeed;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);

        ArrayList<HashMap<String, Object>> listData = new ArrayList<>();
        listData.add(new HashMap<String, Object>());
        listData.add(new HashMap<String, Object>());
        listData.add(new HashMap<String, Object>());

        CustomAdapter customAdapter = new CustomAdapter(getApplicationContext(), listData);

        listView.setAdapter(customAdapter);
    }
}
