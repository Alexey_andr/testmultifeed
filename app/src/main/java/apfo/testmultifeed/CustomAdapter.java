package apfo.testmultifeed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;


public class CustomAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater mInflater;
    private ArrayList<HashMap<String, Object>> itemsArrayList;



    public CustomAdapter(Context context, ArrayList<HashMap<String, Object>> itemsArrayList){
        this.context = context;
        this.itemsArrayList = itemsArrayList;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return itemsArrayList.size();
    }

    @Override
    public HashMap<String, Object> getItem(int position) {
        return itemsArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HashMap<String, Object> item = getItem(position);

        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.item, parent, false);
        }

        View dataView = mInflater.inflate(R.layout.data_subitem, null);

        LinearLayout itemLayout = (LinearLayout) view.findViewById(R.id.linearLayout);

        if (dataView.getParent() != null){
            ((ViewGroup) dataView.getParent()).removeView(dataView);
        }

        itemLayout.addView(dataView);

        for (int i = 0; i < new Random().nextInt(10); i++){
            View newsView = mInflater.inflate(R.layout.news_subitem, null);

            if (newsView.getParent() != null){
                ((ViewGroup) newsView.getParent()).removeView(newsView);
            }

            itemLayout.addView(newsView);
        }


        return view;
    }
}
